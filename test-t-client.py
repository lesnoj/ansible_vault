#!/usr/bin/env python

import argparse
import sys
import os

# mangle_args=('vault-id','extra-vars')
# arguments=['--'+arg if arg in mangle_args else arg for arg in sys.argv[1:]]
# print(arguments)


def createParser():
    parser=argparse.ArgumentParser("Get vault password")
    parser.add_argument('--vault-id', dest='vault_id')
    # parser.add_argument('--extra-vars', dest='extra_vars')

    return parser


if __name__ == '__main__':
    parser = createParser()
    args=parser.parse_args()

    path_project = "/home/user/my_project/ansible_project"
    path_secret = path_project + '/pass_files'
    secret_words = args.vault_id
    secret_list = secret_words.split('-') if '-' in secret_words else secret_words.split()
    
    try:
        if os.environ["MY_JENK"] is not None:
            get_var = os.environ["MY_JENK"]
            with open(f"{path_project}/get.txt", 'w') as w:
                w.write(get_var)
    except KeyError:
        pass

    # if args.vault_id is not None and len(secret_list) > 1:
    #     with open(f"{path_project}/get_vars_from_jenkins.txt", 'w') as w:
    #         w.write(secret_list[1])

    with open(f"{path_secret}/.{secret_list[0]}_pass") as f:
    	password = f.read()

    print(password)
