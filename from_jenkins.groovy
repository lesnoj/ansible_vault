currentBuild.description = """
    ansible: script to run
    agent: ${env.node_name}
"""
node("ubuntu_server") {
    try {
        stage("FILES PERMISSIONS") {
            sh "ls -la ${ansible_path}/get_vars_from_jenkins.txt"
            //sh "export MY_JENK"
            //sh "python ${ansible_path}/test.py"
            //sh "python ${ansible_path}/test-t-client.py"
        }
        stage("TEST") {
            echo "Test string"
            // sh "python /home/user/my_project/ansible_linux_oracle/pass-client.py"
            // sh "export MY_JENK"
            // sh "export MY_JENK_TWO"
            sh "export J_VAR"
            sh """ansible-playbook -vvvv -i ${ansible_path}/inventory/dev/inventory.yml ${ansible_path}/playbook.yml --vault-id dev@${ansible_path}/work-client.py"""
            sh "pwd"
            //get_secret = "${ansible_password}"
            //echo "${get_secret}"
        }
        stage("LOOK FOR ENVIROMENT VARIABLES") {
            sh "printenv | sort"
        }
    }
    catch (e) {
        currentBuild.result = 'FAILURE'
    }
}
