#!/usr/bin/env python

import argparse
import sys
import os

def createParser():
    parser=argparse.ArgumentParser("Get vault password")
    parser.add_argument('--vault-id', dest='vault_id')

    return parser


if __name__ == '__main__':
    parser = createParser()
    args=parser.parse_args()

    path_project = "/home/user/my_project/ansible_project"
    path_secret = path_project + '/pass_files'
    secret_word = args.vault_id
    
    try:
        if os.environ["MY_JENK_ONE"] is not None:
            get_var_one = os.environ["MY_JENK_ONE"]
            with open(f"{path_project}/get_one.txt", 'w') as w:
                w.write(get_var_one)
    except KeyError:
        pass

    try:
        if os.environ["MY_JENK_TWO"] is not None:
            get_var_two = os.environ["MY_JENK_TWO"]
            with open(f"{path_project}/get_two.txt", 'w') as w:
                w.write(get_var_two)
    except KeyError:
        pass

    with open(f"{path_secret}/.{secret_word}_pass") as f:
    	password = f.read()

    print(password)
